# -*- coding: utf-8 -*-
"""Application configuration."""
import os
import logging
from .mibmap import MIBMAP as M


class Config(object):
    """Base configuration."""

    SECRET_KEY = os.environ.get('DASHBOARD_SECRET', 'secret-key')
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # this dir
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    LOG_FORMAT = '%(levelname)-6s %(asctime)s %(name)-10s %(message)s'
    LOG_LEVEL = logging.INFO
    MIBMAP = M
    SLEEP_INTERVAL = 60  # seconds
    MIKROTIK_ADDRESS = '192.168.88.1'
    MIKROTIK_PORT = 161
    GRAPHITE_ADDRESS = 'graphite'
    GRAPHITE_PORT = 2003


class ProdConfig(Config):
    """Production configuration."""
    ENV = 'prod'
    DEBUG = False
    LOG_LEVEL = logging.INFO


class DevConfig(Config):
    """Development configuration."""
    ENV = 'dev'
    DEBUG = True
    LOG_LEVEL = logging.INFO
