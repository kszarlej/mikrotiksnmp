import logging
import threading
import time
from datetime import datetime
import json

# pysnmp for TRAPS
from pysnmp.entity import engine, config
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.entity.rfc3413 import ntfrcv
from easysnmp.exceptions import EasySNMPTimeoutError

from mikrotiksnmp.snmputils import snmpget, snmpwalk, easyget, easywalk
import mikrotiksnmp.mibmap
from mikrotiksnmp.graphite import Graphite


# control when threads should shutdown
threads_doexit = 0

logger = logging.getLogger(__name__)


class InterfacesWorker(threading.Thread):
    """ Scans IFMIB. """

    interfaces_oid = "IF-MIB::ifIndex"
    mibs = {
        'ifDescr': 'IF-MIB::ifDescr',
        'mtu': 'IF-MIB::ifMtu',
        'octets_out': 'IF-MIB::ifOutOctets',
        'octets_in': 'IF-MIB::ifInOctets',
        'packets_in': 'IF-MIB::ifInUcastPkts',
        'packets_out': 'IF-MIB::ifOutUcastPkts',
        'oper_status': 'IF-MIB::ifOperStatus',
        'type': 'IF-MIB::ifType',
        'speed': 'IF-MIB::ifSpeed'
        }

    def __init__(self, thrname, sleep_interval, mikrotik_address, mikrotik_port):
        threading.Thread.__init__(self)
        self.name = thrname
        self.sleep_interval = sleep_interval
        self.mikrotik_address = mikrotik_address
        self.mikrotik_port = mikrotik_port

    def run(self):
        while True:
            try:
                logger.info('Starting %s worker', self.name)

                interfaces = {}
                info = {}

                for idx in easywalk(InterfacesWorker.interfaces_oid, self.mikrotik_address, self.mikrotik_port):
                    interfaces[idx.value] = {}

                for readable_name, oid in InterfacesWorker.mibs.items():
                    for snmp_var in easywalk(oid, self.mikrotik_address, self.mikrotik_port):
                        interfaces[snmp_var.oid_index][readable_name] = (snmp_var.value, snmp_var.snmp_type)

                mikrotiksnmp.mibmap.MIBMAP['interfaces'] = interfaces

                time.sleep(self.sleep_interval)
            except EasySNMPTimeoutError:
                logger.error('Timemout while contacting with router in %s thread', self.name)
                time.sleep(self.sleep_interval)
                continue


class SysteminfoWorker(threading.Thread):
    """ Gets some router info. """

    system_mibs = [
        'SNMPv2-MIB::sysName',
        'SNMPv2-MIB::sysDescr',
        'SNMPv2-MIB::sysUpTime',
        'SNMPv2-MIB::sysLocation',
        'HOST-RESOURCES-MIB::hrProcessorLoad',
        'HOST-RESOURCES-MIB::hrMemorySize',
        'HOST-RESOURCES-MIB::hrSystemDate'
#        'SNMPv2-SMI::mib-2.47.1.1.1.1.2.65536'
        ]

    hr_storage_index = "HOST-RESOURCES-MIB::hrStorageIndex"
    hr_storage_mibs = {
        'type': 'HOST-RESOURCES-MIB::hrStorageType',
        'descr': 'HOST-RESOURCES-MIB::hrStorageDescr',
        'allocation_units': 'HOST-RESOURCES-MIB::hrStorageAllocationUnits',
        'storage_size': 'HOST-RESOURCES-MIB::hrStorageSize',
        'storage_used': 'HOST-RESOURCES-MIB::hrStorageUsed',
        'allocation_failuers': 'HOST-RESOURCES-MIB::hrStorageAllocationFailures'
    }

    def __init__(self, thrname, sleep_interval, mikrotik_address, mikrotik_port):
        threading.Thread.__init__(self)
        self.name = thrname
        self.sleep_interval = sleep_interval
        self.mikrotik_address = mikrotik_address
        self.mikrotik_port = mikrotik_port

    def run(self):
        while True:
            try:
                logger.info('Starting %s worker', self.name)

                system_keys = ['sysName', 'sysDescr', 'sysUptime', 'sysLocation',
                               'hrProcessorLoad', 'hrMemorySize', 'hrSystemDate']
                               #'operatingSystem']
                system_values = []

                host_resources_storage = {}

                for oid in SysteminfoWorker.system_mibs:
                    for snmp_var in easywalk(oid, self.mikrotik_address, self.mikrotik_port):
                        system_values.append((snmp_var.value, snmp_var.snmp_type))

                sysinfo = dict(zip(system_keys, system_values))
                mikrotiksnmp.mibmap.MIBMAP['sysinfo'] = sysinfo

                for idx in easywalk(SysteminfoWorker.hr_storage_index, self.mikrotik_address, self.mikrotik_port):
                    host_resources_storage[idx.value] = {}

                for readable_name, oid in SysteminfoWorker.hr_storage_mibs.items():
                    for snmp_var in easywalk(oid, self.mikrotik_address, self.mikrotik_port):
                        host_resources_storage[snmp_var.oid_index][readable_name] = (snmp_var.value, snmp_var.snmp_type)

                mikrotiksnmp.mibmap.MIBMAP['storage'] = host_resources_storage
                time.sleep(self.sleep_interval)
            except EasySNMPTimeoutError:
                logger.error('Timemout while contacting with router in %s thread', self.name)
                time.sleep(self.sleep_interval)
                continue


class GraphiteWorker(threading.Thread):
    """ Populates Graphite with data from MIBMAP. """

    def __init__(self, thrname, graphite_addr, graphite_port, sleep_interval):
        threading.Thread.__init__(self)
        self.name = thrname
        self.graphite = Graphite(graphite_addr, graphite_port)
        self.sleep_interval = sleep_interval

    def run(self):
        while True:
            try:
                # Interfaces
                metrics = []
                for _, ifdata in mikrotiksnmp.mibmap.MIBMAP['interfaces'].items():
                    ifname = ifdata['ifDescr'][0]

                    for metric, snmp_var in ifdata.items():
                        if metric in ['packets_out', 'packets_in', 'octets_in', 'octets_out']:
                            metrics.append("interfaces.{0}.{1} {2} {3}".format(ifname, metric,
                                                                                         snmp_var[0], time.time()))

                # CPU
                processor_load = mikrotiksnmp.mibmap.MIBMAP['sysinfo']['hrProcessorLoad'][0]
                metrics.append("processor.load {0} {1}".format(processor_load, time.time()))

                # Memory
                for _, ifdata in mikrotiksnmp.mibmap.MIBMAP['storage'].items():
                    if ifdata['descr'][0] == 'main memory':
                        metrics.append("memory.used {0} {1}".format(ifdata['storage_used'][0], time.time()))
                        metrics.append("memory.size {0} {1}".format(ifdata['storage_size'][0], time.time()))

                    if ifdata['descr'][0] == 'disk: system':
                        metrics.append("storage.used {0} {1}".format(ifdata['storage_used'][0], time.time()))
                        metrics.append("storage.size {0} {1}".format(ifdata['storage_size'][0], time.time()))

                self.graphite.send(metrics)
                time.sleep(self.sleep_interval)
            except KeyError:
                continue


class TrapWorker(threading.Thread):
    """ Receives notification and saves them in dict. """

    def __init__(self, thrname):
        threading.Thread.__init__(self)
        self.name = thrname

    def run(self):
        # Callback function for receiving notifications
        # noinspection PyUnusedLocal,PyUnusedLocal,PyUnusedLocal
        snmpEngine = engine.SnmpEngine()

        config.addTransport(
            snmpEngine,
            udp.domainName + (1,),
            udp.UdpTransport().openServerMode(('0.0.0.0', 162))
        )

        config.addV1System(snmpEngine, 'my-area', 'public')

        def cbFun(snmpEngine, stateReference, contextEngineId, contextName,
                  varBinds, cbCtx):
            print('Notification from ContextEngineId "%s", ContextName "%s"' % (contextEngineId.prettyPrint(),
                                                                                contextName.prettyPrint()))

            trap = {}
            for name, val in varBinds:
                name = name.prettyPrint()
                val = val.prettyPrint()

                if name == "1.3.6.1.2.1.2.2.1.1":
                    trap['ifindex'] = val
                    trap['ifdescr'] = mikrotiksnmp.mibmap.MIBMAP['interfaces'][val]['ifDescr'][0]

                if name == "1.3.6.1.2.1.2.2.1.7":
                    trap['adminstatus'] = val

                if name == "1.3.6.1.2.1.2.2.1.8":
                    trap['operstatus'] = val

            trap['time'] = str(datetime.fromtimestamp(time.time()))
            mikrotiksnmp.mibmap.TRAPS.append(trap)

        # Register SNMP Application at the SNMP engine
        ntfrcv.NotificationReceiver(snmpEngine, cbFun)

        snmpEngine.transportDispatcher.jobStarted(1)  # this job would never finish
        # Run I/O dispatcher which would receive queries and send confirmations
        try:
            snmpEngine.transportDispatcher.runDispatcher()
        except Exception as e:
            print(e)
            snmpEngine.transportDispatcher.closeDispatcher()
            raise
