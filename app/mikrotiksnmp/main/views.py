# -*- coding: utf-8 -*-
from flask import current_app as app
import logging
from flask import Blueprint, render_template, flash, redirect, url_for, request, abort
from mikrotiksnmp.nav import nav
import json
from mikrotiksnmp.mibmap import MIBMAP, TRAPS
from mikrotiksnmp.snmputils import easyset
from subprocess import Popen, PIPE

blueprint = Blueprint('main', __name__,
                      static_folder='../static',
                      template_folder='../templates')


@blueprint.route('/', methods=['GET'])
def overview():
    try:
        sysinfo = MIBMAP['sysinfo']
    except KeyError:
        sysinfo = {}

    try:
        storage = MIBMAP['storage']
    except KeyError:
        storage = {}

    return render_template('overview.html', sysinfo=sysinfo,
                           storage=storage)


@blueprint.route('/interfaces', methods=['GET'])
def interfaces():
    return render_template('interfaces.html', interfaces=MIBMAP['interfaces'])


@blueprint.route('/traps', methods=['GET'])
def traps():
    return render_template('traps.html', traps=TRAPS)


@blueprint.route('/restart_router', methods=['GET'])
def restart_router():
    oid = '.1.3.6.1.4.1.14988.1.1.7.1.0'
    command = ['snmpset', '-v2c', '-c', 'public', app.config.get('MIKROTIK_ADDRESS'), oid, 's', '1']
    try:
        proc = Popen(command, stdout=PIPE, stderr=PIPE, stdin=PIPE)
        proc.communicate()
        rc = proc.returncode
        if rc == 0:
            flash('Router should restart now.', 'success')
        else:
            flash('Unable to restart router.', 'error')
    except OSError as e:
        print("{0}: {1}".format(command, e))

    return redirect(url_for('main.overview'))
