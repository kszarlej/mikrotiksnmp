from flask import Flask
from mikrotiksnmp import main
import mikrotiksnmp.workers as workers
from flask_bootstrap import Bootstrap
from mikrotiksnmp.extensions import nav


def init_extensions(app):
    nav.init_app(app)


def init_blueprints(app):
    app.register_blueprint(main.views.blueprint)


def start_workers(config):
    interfaces_worker = workers.InterfacesWorker("interfaces_worker",
                                                 sleep_interval=config.SLEEP_INTERVAL,
                                                 mikrotik_address=config.MIKROTIK_ADDRESS,
                                                 mikrotik_port=config.MIKROTIK_PORT)

    interfaces_worker.daemon = False
    interfaces_worker.start()

    sysinfo_worker = workers.SysteminfoWorker("sysinfo_worker",
                                              sleep_interval=config.SLEEP_INTERVAL,
                                              mikrotik_address=config.MIKROTIK_ADDRESS,
                                              mikrotik_port=config.MIKROTIK_PORT)

    sysinfo_worker.daemon = False
    sysinfo_worker.start()

    graphiteworker = workers.GraphiteWorker("graphiteworker",
                                            sleep_interval=config.SLEEP_INTERVAL,
                                            graphite_addr=config.GRAPHITE_ADDRESS,
                                            graphite_port=config.GRAPHITE_PORT)
    graphiteworker.daemon = False
    graphiteworker.start()

    trapworker = workers.TrapWorker("trapworker")
    trapworker.daemon = False
    trapworker.start()


def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)

    Bootstrap(app)
    init_extensions(app)
    init_blueprints(app)
    start_workers(config)

    return app
