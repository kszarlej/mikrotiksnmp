import logging
from .snmputils import snmpwalk, snmpget, easywalk, easyget

logger = logging.getLogger(__name__)


def init_routerinfo(mikrotik_address, mikrotik_port):
    sys_descr = easyget(".1.3.6.1.2.1.1.1.0", mikrotik_address, mikrotik_port)
    sys_contact = easyget(".1.3.6.1.2.1.1.4.0", mikrotik_address, mikrotik_port)
    sys_name = easyget(".1.3.6.1.2.1.1.5.0", mikrotik_address, mikrotik_port)

    SYSINFO = {}
    SYSINFO['description'] = (sys_descr.value, sys_descr.snmp_type)
    SYSINFO['contact'] = (sys_contact.value, sys_contact.snmp_type)
    SYSINFO['sys_name'] = (sys_name.value, sys_descr.snmp_type)

    return SYSINFO
