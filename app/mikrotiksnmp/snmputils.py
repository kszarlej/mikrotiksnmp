import pysnmp.hlapi as snmp
import logging
from . import mibmap
from easysnmp import Session

logger = logging.getLogger(__name__)


def snmpget(oid, address, port):
    logger.debug("Getting oid %s on host %s:%s", oid, address, port)
    errorIndication, errorStatus, errorIndex, varBinds = next(
        snmp.getCmd(snmp.SnmpEngine(),
                    snmp.CommunityData('public'),
                    snmp.UdpTransportTarget((address, port)),
                    snmp.ContextData(),
                    snmp.ObjectType(snmp.ObjectIdentity(oid)))
    )

    if errorIndication:
        logger.error("An error occured on snmpget: %s", errorIndication)
    elif errorStatus:
        logger.error('%s at %s', errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
    else:
        for varBind in varBinds:
            varSplitted = ' = '.join([x.prettyPrint() for x in varBind]).split(' = ')
            return({"metric": varSplitted[0], "value": varSplitted[1]})


def snmpwalk(oid, address, port):
    logger.debug("Walking oid %s on host %s:%s", oid, address, port)
    for (errorIndication,
         errorStatus,
         errorIndex,
         varBinds) in snmp.nextCmd(snmp.SnmpEngine(),
                                   snmp.CommunityData('public'),
                                   snmp.UdpTransportTarget((address, port)),
                                   snmp.ContextData(),
                                   snmp.ObjectType(snmp.ObjectIdentity(oid))):

        if errorIndication:
            logger.error("An error occured on snmpwalk: %s", errorIndication)
            break
        elif errorStatus:
            logger.error('%s at %s', errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?')
            break
        else:
            for varBind in varBinds:
                varSplitted = ' = '.join([x.prettyPrint() for x in varBind]).split(' = ')
                yield({"metric": varSplitted[0], "value": varSplitted[1]})


def easywalk(oid, address, port):
    logger.debug("EasyWalking oid %s on host %s:%s", oid, address, port)
    session = Session(hostname="{0}:{1}".format(address, port), community='public', version=2)
    return session.walk(oid)


def easyget(oid, address, port):
    logger.debug("EasyGet oid %s on host %s:%s", oid, address, port)
    session = Session(hostname="{0}:{1}".format(address, port), community='public', version=2)
    return session.get(oid)


def easyset(oid, address, port, value):
    print("EasySet oid %s on host %s:%s", oid, address, port)
    session = Session(hostname="{0}:{1}".format(address, port), community='public', version=2)
    return session.set(oid, 'asd')


def mibmapget(level):
    logger.debug("Trying to obtain mibmap level %s", level)
    if level in mibmap.MIBMAP:
        return mibmap.MIBMAP[level]
    return {"asd"}
