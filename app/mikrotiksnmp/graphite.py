import socket


class Graphite:

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def send(self, metrics):
        message = '\n'.join(metrics) + '\n'
        sock = socket.socket()
        try:
            sock.connect((self.host, self.port))
            sock.sendall(message.encode())
            sock.close()
        except socket.error as exc:
            print("Can't connect to Graphite on {0}:{1}. Exception: {2}"
                  .format(self.host, self.port, str(exc)))
            return
