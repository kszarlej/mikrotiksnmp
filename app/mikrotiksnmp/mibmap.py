MIBMAP = {}
TRAPS = []

GRAPHITE_MAP = {
    "IFMIB": {
        "pattern": "interfaces.{descr}.{metric} {val} {time}",
        "to_send": ['octets_in', 'octets_out', 'mtu', 'oper_status', 'packets_in', 'packets_out']
    }
}
