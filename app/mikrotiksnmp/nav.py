from flask_nav.elements import Navbar, View
from mikrotiksnmp.extensions import nav

nav.register_element('frontend_top', Navbar(
    View('Overview', 'main.overview'),
    View('Interfaces', 'main.interfaces'),
    View('Traps', 'main.traps')))
