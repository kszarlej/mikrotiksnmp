# -*- coding: utf-8 -*-
"""Create an application instance."""
import os
import sys
from mikrotiksnmp.app import create_app
from mikrotiksnmp.config import ProdConfig, DevConfig

env = os.getenv("FLASK_ENV", 'dev')
if env == 'dev':
    cfg = DevConfig
elif env == 'prod':
    cfg = ProdConfig
else:
    print('Wrong environment')
    sys.exit(1)

print("*** Starting application in {0} mode ***".format(env))
app = create_app(cfg)
