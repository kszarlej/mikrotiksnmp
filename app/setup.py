from setuptools import setup

setup(
    version='0.0.1',
    name='mikrotiksnmp',
    packages=['mikrotiksnmp'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
