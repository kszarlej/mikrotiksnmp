FROM python:3.5

ENV APP_DIR /app
ENV PYTHONPATH /app

RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}

RUN sed -i '/jessie main/d' /etc/apt/sources.list \
    && echo 'deb http://deb.debian.org/debian jessie main non-free' >> /etc/apt/sources.list \
    && apt-get update -qq \
    && apt-get install -y libsnmp-dev snmp snmp-mibs-downloader \
    && rm -rf /var/lib/apt/lists/* \
    && sed -i '/mibs /d' /etc/snmp/snmp.conf

ADD ./app/requirements.txt ${APP_DIR}/requirements.txt
RUN pip install --upgrade pip && pip install -r ${APP_DIR}/requirements.txt

ADD /entrypoint.sh /entrypoint.sh
ADD ./app ${APP_DIR}

ENTRYPOINT ["/entrypoint.sh"]
